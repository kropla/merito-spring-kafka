# Read Me First
Aplikacja pokazowa wykorzystania kafki do produkowania i odczytu danych pozyskanych z zewnętrznego portalu generującego dane użytkowników (user).


Aplikacja zaimplementowana w oparciu o Springboot.

## Pobieranie danych z serwisu zewnętrznego
* Wykorzystano scheduler odpalający co jakiś czas (5s) proces pozyskiwania danych z serwisu zewnętrznęgo.
`https://random-data-api.com`
* Scheduler co pewien czas tworzy i uruchamia job ([ProducerJob](src/main/java/dev/merito/spring/kafka/producer/ProducerJob.java))
* Job wywołuję komponent [RestWebClient](src/main/java/dev/merito/spring/kafka/producer/RestWebClient.java), który używając komponent `restTemplate` umożliwia pobranie randomowo wygenerowanych danych uzytkownika uderzając pod URL `https://random-data-api.com/api/v2/users`

## Produkowanie wiadomości
* Dane o użytkowniku są następnie wysyłane przez producenta [KafkaProducer](src/main/java/dev/merito/spring/kafka/producer/KafkaProducer.java) do serwera kafki.

## Konsumowanie wiadomości
* Konsument kafki ([KafkaConsumer](src/main/java/dev/merito/spring/kafka/consumer/KafkaConsumer.java) nasłuchuje na topiku czy są nowe wiadomości. Po ich otrzymaniu tworzony jest obiekt [RandomUser](src/main/java/dev/merito/spring/kafka/model/RandomUser.java), który następnie zapisywany jest w bazie danych postgres.
* Dla ułatwienia ten sam model jest używany do zapisu do bazy danych oraz wyświetlenia danych poprzez kontroler - nie jest to zalecane rozwiązanie (model bazy danych mpowinien być wyodrębniony od modelu prezentowanego w UI aplikacji) 

## API aplikacji
* Aplikacja wystawia API w postaci endpointów REST 
  * `GET /user` dla pobrania listy wszystkich zapisanych użytkowników 
  * `GET /user/{id}` dla wyświetlenia detali pojedynczego użytkownika.
* wszystkie endpointy dostępne są z poziomu strony swaggera: `http://localhost:8080/swagger-ui/index.html`
![swaggerPage.png](doc/swaggerPage.png)

## Uruchomienie aplikacji
W celu uruchomienia aplikacji należy upewnić się, że zależne komponenty działają i są dostępne:
1) kafka
2) postgres

Oba zależne serwisy można uruchomić używająć skryptu [compose.yml](compose.yml) (uruchamiając cały skrypt lub poszczególne serwisy)

Kiedy serwisy zależne są uruchomionę aplikację należy uruchomić z klasy [SpringKafkaApplication.java](src/main/java/dev/merito/spring/kafka/SpringKafkaApplication.java) jednym z poniższych sposobów:
- z menu kontekstowego - po kliknięciu prawym przyciskiem myszy na tej klasie - proszę wybrać opcję `Run`. 
![appRun1.png](doc/appRun1.png)
- uruchomienie aplikacji bezpośrednio z w/w klasy 
![appRun2.png](doc/appRun2.png)
- użycie odpowiedniego tasku gradle (`bootRun`)
![appRun3.png](doc/appRun3.png)