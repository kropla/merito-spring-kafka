package dev.merito.spring.kafka.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.Data;

import java.util.UUID;

@Data
@Entity
public class RandomUser {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID userId;
    @JsonProperty("id")
    private String extId;
    private String uid;
    private String password;

    @JsonProperty("first_name")
    private String firstName;

    @JsonProperty("last_name")
    private String lastName;
    private String username;
    private String email;

}
