package dev.merito.spring.kafka.service;

import dev.merito.spring.kafka.model.UserRepository;
import dev.merito.spring.kafka.model.RandomUser;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class UserService {
    private UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public RandomUser create(RandomUser randomUser) {

        return userRepository.save(randomUser);
    }

    public List<RandomUser> getAll() {
        return userRepository.findAll();
    }

    public Optional<RandomUser> getUser(UUID id) {
        return userRepository.findById(id);
    }
}
