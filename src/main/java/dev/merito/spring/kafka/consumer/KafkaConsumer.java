package dev.merito.spring.kafka.consumer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import dev.merito.spring.kafka.model.RandomUser;
import dev.merito.spring.kafka.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class KafkaConsumer {

    private ObjectMapper objectMapper;
    private UserService userService;
    public KafkaConsumer(ObjectMapper objectMapper, UserService userService) {
        this.objectMapper = objectMapper;
        this.userService = userService;
    }

    @KafkaListener(id = "id1", topicPattern = "new-spring-topic-1")
    public void listen(@Payload String payload, @Header(KafkaHeaders.RECEIVED_PARTITION) int partition,
                       @Header(KafkaHeaders.OFFSET) long offset
    ) throws JsonProcessingException {

        RandomUser randomUser = objectMapper.readValue(payload, RandomUser.class);

        log.info("offset: {}, partition: {}, user: {}", offset, partition, randomUser);
        userService.create(randomUser);
    }

}
