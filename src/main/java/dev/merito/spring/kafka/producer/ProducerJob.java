package dev.merito.spring.kafka.producer;


import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class ProducerJob {

    private RestWebClient restWebClient;

    public ProducerJob(RestWebClient restWebClient) {
        this.restWebClient = restWebClient;
    }

    @Scheduled(fixedDelay = 5000)
    public void scheduledFixedDelayTask() throws Exception {
        restWebClient.run(new String[] {});
    }
}
