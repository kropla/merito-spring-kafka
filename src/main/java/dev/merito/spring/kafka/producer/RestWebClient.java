package dev.merito.spring.kafka.producer;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@Slf4j
@Component
public class RestWebClient implements CommandLineRunner {

    private RestTemplate restTemplate;

    private KafkaProducer kafkaProducer;

    public RestWebClient(RestTemplate restTemplate, KafkaProducer kafkaProducer) {
        this.restTemplate = restTemplate;
        this.kafkaProducer = kafkaProducer;
    }

    @Override
    public void run(String... args) throws Exception {
        try{
            ResponseEntity<String> entity = restTemplate.getForEntity("https://random-data-api.com/api/v2/users", String.class);

            kafkaProducer.send(entity.getBody());
        } catch (HttpClientErrorException ex) {
            log.info("Can't get entity now. Skipping... {}", ex.getMessage());
        }
    }
}
